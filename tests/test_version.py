import pytest
from flask import Flask

app = Flask(__name__)

@pytest.fixture
def client():
    client = app.test_client()
    yield client


def test_version(client):
    req = client.get('/')
    assert req.data == app.version
